using UnityEngine;

public class CactusController : MonoBehaviour
{
    public CactusGenerator cactusGenerator;
    
    void Update()
    {
        transform.Translate(Vector2.left * cactusGenerator.currentSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CactusTrigger"))
        {
            cactusGenerator.GenerateCactusWithRandomGap();
        }

        if (collision.gameObject.CompareTag("CactusEraser"))
        {
            Destroy(this.gameObject);
        }
    }
}
