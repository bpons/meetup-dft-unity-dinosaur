using UnityEngine;

public class CactusGenerator : MonoBehaviour
{
    public GameObject cactus;

    public float minSpeed;
    public float maxSpeed;
    public float currentSpeed;

    public float speedMultiplier;
    
    void Awake()
    {
        currentSpeed = minSpeed;
        GenerateCactus();
    }

    public void GenerateCactusWithRandomGap()
    {
        float randomWait = Random.Range(0.1f, 1.2f);
        Invoke("GenerateCactus", randomWait);
    }
    
    public void GenerateCactus()
    {
        GameObject cactusCreated = Instantiate(cactus, transform.position, transform.rotation);
        cactusCreated.GetComponent<CactusController>().cactusGenerator = this;
    }
    
    void Update()
    {
        if (currentSpeed < maxSpeed)
        {
            currentSpeed += speedMultiplier;
        }
    }
}
