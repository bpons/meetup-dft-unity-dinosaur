using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Text scoreText;
    public float jumpForce;
    public float score;
    
    private Rigidbody2D rigidBody;
    
    private bool isGrounded = false;
    private bool isAlive = true;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        score = 0f;
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded)
            {
                rigidBody.AddForce(Vector2.up * jumpForce);
                isGrounded = false;
            }
        }
        
        if (isAlive)
        {
            score += Time.deltaTime * 4;
            scoreText.text = "Puntaje: " + score.ToString("F");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            if (!isGrounded)
            {
                isGrounded = true;
            }
        }
        
        if (collision.gameObject.CompareTag("Cactus"))
        {
            isAlive = false;
            Time.timeScale = 0;
        }
    }
}
